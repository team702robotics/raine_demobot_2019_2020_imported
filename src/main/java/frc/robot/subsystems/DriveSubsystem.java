/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;
import edu.wpi.first.wpilibj.Joystick;
import frc.robot.RobotMap;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import frc.robot.commands.DriveCommand;


/**0
 * Add your docs here.
 */
public class DriveSubsystem extends Subsystem {
  CANSparkMax SparkA; // left mid
  CANSparkMax SparkB; // left back
  CANSparkMax SparkC; // right mid
  CANSparkMax SparkD; // right back
  WPI_TalonSRX TalonA; // right front
  WPI_TalonSRX TalonB; // left front
  //WPI_TalonFX falconA; // right front
  //WPI_TalonFX falconB; // left front



  //tank drive
  /*`
  DifferentialDrive leftDrive;
  DifferentialDrive rightDrive;
  */
    //arcade drive
  
  DifferentialDrive frontDrive;
  DifferentialDrive midDrive;
  DifferentialDrive backDrive;
  
  double leftPos;
  double rightPos;

  public DriveSubsystem(){
    
    //Declaring Sparks
    SparkA = new CANSparkMax (RobotMap.SparkA, MotorType.kBrushed); // left mid
    SparkB = new CANSparkMax (RobotMap.SparkB, MotorType.kBrushed); // left back;
    SparkC = new CANSparkMax (RobotMap.SparkC, MotorType.kBrushed); // right mid
    SparkD = new CANSparkMax (RobotMap.SparkD, MotorType.kBrushed); // right back

    //Declaring Talons
    TalonA = new WPI_TalonSRX(RobotMap.TalonA);
    TalonB = new WPI_TalonSRX(RobotMap.TalonB);

    
    /*
    leftPos = 0;
    rightPos = 0;
    //tank drive
    //Declaring Drive
    
    leftDrive = new DifferentialDrive(TalonA, TalonA);
    rightDrive = new DifferentialDrive(TalonB, TalonB);
    SparkA.setIdleMode(IdleMode.kBrake);
    SparkB.setIdleMode(IdleMode.kBrake);
    SparkC.setIdleMode(IdleMode.kBrake);
    SparkD.setIdleMode(IdleMode.kBrake);
    */

    //arcade drive
    /*
    frontDrive = new DifferentialDrive(TalonA, TalonB);
    //frontDrive = new DifferentialDrive(falconA, falconB);
    midDrive = new DifferentialDrive(SparkA , SparkC);
    //backDrive = new DifferentialDrive(SparkB, SparkD);
    TalonA.setInverted(true);
    TalonB.setInverted(true);
    //falconA.setInverted(true);
    //falconB.setInverted(true);
    */
    frontDrive = new DifferentialDrive(TalonA, TalonB);
    midDrive = new DifferentialDrive(SparkA, SparkC);
    backDrive = new DifferentialDrive(SparkB, SparkD);
  }
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    setDefaultCommand(new DriveCommand());
  }

  public void drive(Joystick a){
    midDrive.tankDrive(a.getRawAxis(RobotMap.lsYA), a.getRawAxis(RobotMap.rsYA));
    backDrive.tankDrive(a.getRawAxis(RobotMap.lsYA), a.getRawAxis(RobotMap.rsYA));
    /*
    if(a.getRawAxis(RobotMap.triggerLeft)>0.1)
    {
      if(a.getRawAxis(RobotMap.triggerRight)>0.1)
      {
          frontDrive.tankDrive(a.getRawAxis(RobotMap.lsYA), a.getRawAxis(RobotMap.rsYA));
          midDrive.tankDrive(a.getRawAxis(RobotMap.lsYA), a.getRawAxis(RobotMap.rsYA));
          backDrive.tankDrive(a.getRawAxis(RobotMap.lsYA), a.getRawAxis(RobotMap.rsYA));
      } else{
        frontDrive.tankDrive(0.5*a.getRawAxis(RobotMap.lsYA), 0.5*a.getRawAxis(RobotMap.rsYA));
        midDrive.tankDrive(0.5*a.getRawAxis(RobotMap.lsYA), 0.5*a.getRawAxis(RobotMap.rsYA));
        backDrive.tankDrive(0.5*a.getRawAxis(RobotMap.lsYA), 0.5*a.getRawAxis(RobotMap.rsYA));

      }
      
    } else {}
      if(a.getRawAxis(RobotMap.triggerRight)>0.1)
      {
      frontDrive.arcadeDrive(a.getRawAxis(RobotMap.lsYA), -1*a.getRawAxis(RobotMap.rsXA));
      midDrive.arcadeDrive(a.getRawAxis(RobotMap.lsYA), -1*a.getRawAxis(RobotMap.rsXA));
      backDrive.arcadeDrive(a.getRawAxis(RobotMap.lsYA), -1*a.getRawAxis(RobotMap.rsXA));
    } else {
      frontDrive.arcadeDrive(0.5*a.getRawAxis(RobotMap.lsYA), -0.5*a.getRawAxis(RobotMap.rsXA));
      midDrive.arcadeDrive(0.5*a.getRawAxis(RobotMap.lsYA), -0.5*a.getRawAxis(RobotMap.rsXA));
      backDrive.arcadeDrive(0.5*a.getRawAxis(RobotMap.lsYA), -0.5*a.getRawAxis(RobotMap.rsXA));
    }
    */
  }

  public void driveForward(double speed) {
    frontDrive.tankDrive(-1*speed, -1*speed);
    midDrive.tankDrive(-1*speed,-1*speed);
    backDrive.tankDrive(-1*speed, -1*speed);
    leftPos+= speed;
    rightPos+= speed;

  }
  public void driveStop() {
    // frontDrive.tankDrive(0, 0);
    midDrive.tankDrive(0, 0);
    backDrive.tankDrive(0, 0);

  }
  public void resetPos() {
    leftPos = 0;
    rightPos = 0;
  }
  public double getLeftPos() {
    return leftPos;
  }
  public double getRightPos() {
    return rightPos;
  }
  public void driveTurn(int direction, double speed) {
    // if direction is -1. go left, if direction is 1 go right
    frontDrive.tankDrive(-1*direction*speed, direction*speed);
    midDrive.tankDrive(direction*speed, direction*speed);
    backDrive.tankDrive(direction*speed, -1*direction*speed);
    leftPos+= direction*speed;
    rightPos+= -1*direction*speed;
  }
}
