/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

public class AutoBallTracking extends Command {
  
  public NetworkTableEntry ball_direction;
  public NetworkTableEntry ball_difference;
  public NetworkTableEntry ball_radius;
  public NetworkTableEntry ball_found;
  // ball_auto
 
  public AutoBallTracking() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.drive);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    NetworkTableInstance inst = NetworkTableInstance.getDefault();
    NetworkTable table = inst.getTable("datatable");
    ball_direction = table.getEntry("ball_direction");
    ball_difference = table.getEntry("ball_difference");
    ball_radius = table.getEntry("ball_radius");
    ball_found = table.getEntry("ball_found");
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    boolean ballFound = ball_found.getBoolean(false);
    if(ballFound) {
      int turnDir = (int) ball_direction.getNumber(0);
      int difference = Math.abs( (int)ball_difference.getNumber(0)); // this is from 40 to 160
      int radius = (int) ball_radius.getNumber(0);

      double speed = difference / 160;
      
      if(turnDir != 0) {
        Robot.drive.driveTurn(turnDir, speed);
      } else if(turnDir == 0) {
        Robot.drive.driveTurn(0, 0.5);
      }
      System.out.println("turnDir:" + turnDir + "\tdifference:" + difference + "\tradius:" + radius);
    } else {
      Robot.drive.driveTurn(0, 0);
      System.out.println("ball not found");
    }
    

  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
